let messages = document.querySelector("#messages")
function add_messages(){
    let message = document.createElement("p")
    let message_area = document.createElement("div")
    let text_messages = document.createElement("div")
    let buttons = document.createElement("div")
    
    message.id = "selecionador"
    let btn_delete = document.createElement("button")
    let btn_edit = document.createElement("button")

    let input = document.querySelector("#input")
    if (input.value != ""){
        message.innerText = input.value
        btn_delete.classList.add("button")
        btn_edit.classList.add("button")

        btn_delete.classList.add("btn_excluir")
        btn_edit.classList.add("btn_editar")

        btn_delete.innerText = "excluir"
        btn_edit.innerText = "Editar"

        buttons.append(btn_delete)
        buttons.append(btn_edit)

        buttons.classList.add("buttons")

        text_messages.classList.add("text_messages")

        text_messages.append(message)
        text_messages.append(buttons)
        let clicou = false
        btn_delete.setAttribute("onClick", "this.parentNode.parentNode.remove()")

        btn_edit.addEventListener("click", (e)=> edit_message(e))

        message_area.id = "message_area"
        message_area.append(text_messages)

        messages.append(message_area)
        input.value = ""
    }else{
        alert("Campo vazio, digite algo")
    }
    
}

function edit_message(e){
    e.target.setAttribute("disabled", "")
    let caixa = document.createElement("div")
    let text_area = document.createElement("textarea")
    text_area.id = "text_area"
    let button = document.createElement("button")
    button.classList.add("button")
    caixa.id = "area"
    button.innerText = "Confirmar"
    button.classList.add("btn_confirmar")
    button.addEventListener("click", (e)=>colapse(e))

    caixa.append(text_area)
    caixa.append(button)

    let box = e.target.parentNode.parentNode

    box.append(caixa)

}

function colapse(e){
    let box = e.target.parentNode.parentNode
    let msg = box.firstChild
    console.log(msg)
    let caixa = box.querySelector("#area")
    let text_area = caixa.querySelector("#text_area")

    msg.innerText = text_area.value
    box.querySelector(".btn_editar").removeAttribute("disabled")
    caixa.remove()
    
}

let btn_send = document.querySelector("#btn_send")
btn_send.addEventListener("click", add_messages)


